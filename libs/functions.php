<?php

require('pdo.php');

class Hosts
{
	// Dans les "bonnes pratiques" on a jamais de variable public dans les classes mais on fait des setter / getter ;)
    public $liste;
     
	public function __construct()
    {
		//$sql =  'SELECT ip, smbName, upnpName FROM hosts ORDER BY ip asc';
		$sql =  'SELECT * FROM hosts ORDER BY ip asc';
		$this->liste = DB::query($sql);
    }
	 
    public function getPseudo()
    {
        return $this->pseudo;
    }
     
    public function setPseudo($nouveauPseudo)
    {
        $this->pseudo = $nouveauPseudo;
    }
}
?>
