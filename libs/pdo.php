<?php

class DBold
{
static $connection;
	function __construct(){
               if(isset(Bdd::$connection)){
                       return true;
               }
		try{
			//MySQL
			#$PARAM_hote='localhost'; // le chemin vers le serveur
			#$PARAM_port='3306';
			#$PARAM_nom_bd='sdz'; // le nom de votre base de donn�es
			#$PARAM_utilisateur='root'; // nom d'utilisateur pour se connecter
			#$PARAM_mot_passe=''; // mot de passe de l'utilisateur pour se connecter
			#$pdo = new PDO('mysql:host='.$PARAM_hote.';port='.$PARAM_port.';dbname='.$PARAM_nom_bd, $PARAM_utilisateur, $PARAM_mot_passe);
			
			//SQLITE3
			$pdo = new PDO('sqlite:'.dirname(__FILE__).'/../test.db');
			$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); // ERRMODE_WARNING | ERRMODE_EXCEPTION | ERRMODE_SILENT
		Bdd::$connection = $pdo;
		}		catch(Exception $e) {
			echo "Impossible d'acc�der � la base de donn�es SQLite : ".$e->getMessage();
			die();
		}
	}
}


class DB { 
    
    private static $objInstance; 
    
    /* 
     * Class Constructor - Create a new database connection if one doesn't exist 
     * Set to private so no-one can create a new instance via ' = new DB();' 
     * Like the constructor, we make __clone private so nobody can clone the instance 
     */ 
    private function __construct() {} 
    private function __clone() {} 
    
    /* 
     * Returns DB instance or create initial connection 
     * @param 
     * @return $objInstance; 
     */ 
    public static function getInstance(  ) { 
            
        if(!self::$objInstance){ 
		
			try{
				//MySQL
				#$PARAM_hote='localhost'; // le chemin vers le serveur
				#$PARAM_port='3306';
				#$PARAM_nom_bd='sdz'; // le nom de votre base de donn�es
				#$PARAM_utilisateur='root'; // nom d'utilisateur pour se connecter
				#$PARAM_mot_passe=''; // mot de passe de l'utilisateur pour se connecter
				#$pdo = new PDO('mysql:host='.$PARAM_hote.';port='.$PARAM_port.';dbname='.$PARAM_nom_bd, $PARAM_utilisateur, $PARAM_mot_passe);
				
				//SQLITE3
				self::$objInstance = new PDO('sqlite:'.dirname(__FILE__).'/../test.db');
				self::$objInstance->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
				self::$objInstance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); // ERRMODE_WARNING | ERRMODE_EXCEPTION | ERRMODE_SILENT
			
			}		catch(Exception $e) {
				Return "Impossible d'acc�der � la base de donn�es SQLite : ".$e->getMessage();
				die();
			}
        }  
        return self::$objInstance; 
    } # end method 
    
    /* 
     * Passes on any static calls to this class onto the singleton PDO instance 
     * @param $chrMethod, $arrArguments 
     * @return $mix 
     */ 
    final public static function __callStatic( $chrMethod, $arrArguments ) { 
            
        $objInstance = self::getInstance(); 
        
        return call_user_func_array(array($objInstance, $chrMethod), $arrArguments); 
    } # end method 
} 
?>