#sqlite3 test.db "select ip from hosts" | xargs echo '{}%'$(timeout 1 nmblookup -A {} | grep '<00> -' | grep -v '<00> - <GROUP>' | cut -d '<' -f 1 | sed 's/^[ \t]//g' | sed  's/[ \t]$//g') | xargs sqlite3 test.db "insert or replace into hosts (ip, smbName, upnpName) values ('$(cut -d '%' 1 {})', $(cut -d  '%' 2{}), (select upnpName from hosts where ip = '$(cut -d '%' 1{})'));"

./list.sh

for ip in `sqlite3 ../test.db "select ip from hosts"`; do
	sqlite3 ../test.db "insert or replace into hosts (ip, smbName, upnpName) values ('$ip', '$(timeout 1 nmblookup -A $ip | grep '<00> -' | grep -v '<00> - <GROUP>' | cut -d '<' -f 1 | sed 's/^[ \t]//g' | sed  's/[ \t]$//g')', '$(avahi-resolve-address $ip | cut -f 2)');"
done

sqlite3 ../test.db "select * from hosts"

